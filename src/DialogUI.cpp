/**

   DialogUI.cpp
   Implementation of DialogUI.h

   Dialog-like UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#include "DialogUI.h"

DialogUI::DialogUI(U8G2 *display, uint8_t startYPos = 0) {
  this->pDisplay = display;
  this->mTopYPos = startYPos + 2;
}

void DialogUI::loop() {
  if (this->mHidden)
    return;

  this->pDisplay->setDrawColor(1);
  this->pDisplay->setFont(DIALOG_UI_ALPHABET_FONT); // width = 6, height = 12

  uint8_t displayWidth = this->pDisplay->getDisplayWidth();
  uint8_t displayHeight = this->pDisplay->getDisplayHeight();
  uint8_t maxCharHeight = this->pDisplay->getMaxCharHeight();
  uint8_t maxCharWidth = this->pDisplay->getMaxCharWidth();

  for (uint8_t i = 0; i < this->mTotalLineShown; i++) {
    uint8_t messageYPos = this->mTopYPos + (maxCharHeight * (i + 1)) - 2;
    this->pDisplay->drawStr(3, messageYPos,
                            this->pMessageLine[this->mStartLine + i]);
    if (i + 1 == this->mTotalLine)
      break;
  }

  uint8_t buttonYPos = this->mTopYPos + (displayHeight - (maxCharHeight + 6));
  uint8_t messageBoxHeight = displayHeight - (maxCharHeight + 4);

  bool endOfScroll =
      ((this->mStartLine + this->mTotalLineShown) == this->mTotalLine) ||
      (this->mTotalLine < this->mTotalLineShown);
  bool outOfShownLine = this->mTotalLine > this->mTotalLineShown;

  this->pDisplay->drawHLine(2, messageBoxHeight, displayWidth - 4);

  if (this->mOkHandler) {
    if (this->mActiveOption == 0 && endOfScroll)
      this->pDisplay->drawBox(
          3, buttonYPos, this->pDisplay->getStrWidth(this->mOkLabel) + 4, 4);
    this->pDisplay->drawStr(5, buttonYPos, this->mOkLabel);
  }

  if (this->mCancelHandler) {
    uint8_t cancelLabelLen = this->pDisplay->getStrWidth(this->mCancelLabel);
    uint8_t cancelXPos = displayWidth - cancelLabelLen - 5;

    if (this->mActiveOption == 1 && endOfScroll)
      this->pDisplay->drawBox(cancelXPos - 2, buttonYPos, cancelLabelLen + 4,
                              4);
    this->pDisplay->drawStr(cancelXPos, buttonYPos, this->mCancelLabel);
  }

  if (outOfShownLine) {
    this->pDisplay->setFont(DIALOG_UI_ICON_FONT);
    this->pDisplay->drawGlyph(
        (displayWidth / 2) - maxCharWidth, messageBoxHeight + maxCharHeight + 1,
        endOfScroll ? DIALOG_UI_THICK_HEAD_ARROW_UP_ICON
                    : DIALOG_UI_THICK_HEAD_ARROW_DOWN_ICON);
  }
} // DialogUI::loop

void DialogUI::prev() {
  if (this->mHidden)
    return;

  if (this->mActiveOption > 0) {
    this->mActiveOption -= 1;
    return;
  }

  if (this->mStartLine > 0 && this->mTotalLine > this->mTotalLineShown) {
    this->mStartLine -= 1;
    return;
  }

} // DialogUI::prev

void DialogUI::next() {
  if (this->mHidden)
    return;

  if (this->mStartLine + this->mTotalLineShown < this->mTotalLine) {
    this->mStartLine += 1;
    return;
  }

  if (this->mActiveOption == 0 && !this->mCancelHandler)
    return;

  if (this->mActiveOption == 1)
    return;

  this->mActiveOption += 1;
} // DialogUI::next

void DialogUI::hide() { this->mHidden = true; }

void DialogUI::show() { this->mHidden = false; }

void DialogUI::reset() {
  this->mActiveOption = 0;
  memset(this->mOkLabel, 0, DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH);
  strcpy(this->mOkLabel, "ok");
  memset(this->mCancelLabel, 0, DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH);
  strcpy(this->mCancelLabel, "cancel");
  this->mCancelHandler = NULL;
  this->mOkHandler = NULL;
  for (int i = 0; i < this->mTotalLine; i++)
    delete[] this->pMessageLine[i];
  this->mTotalLine = 0;
} // DialogUI::reset

void DialogUI::executeOption() {
  bool endOfScroll =
      ((this->mStartLine + this->mTotalLineShown) == this->mTotalLine) ||
      (this->mTotalLine < this->mTotalLineShown);

  if (!endOfScroll)
    return;

  if (this->mActiveOption == 0 && this->mOkHandler) {
    this->mOkHandler();
  }
  if (this->mActiveOption == 1 && this->mCancelHandler) {
    this->mCancelHandler();
  }
} // DialogUI::executeOption

void DialogUI::setMessage(const char *message) {

  size_t messageLen = strlen(message);

  if (messageLen < 1)
    return;

  for (int i = 0; i < this->mTotalLine; i++)
    delete[] this->pMessageLine[i];

  this->pDisplay->setFont(DIALOG_UI_ALPHABET_FONT);

  uint8_t displayWidth = this->pDisplay->getDisplayWidth();
  uint8_t displayHeight = this->pDisplay->getDisplayHeight();
  uint8_t maxCharHeight = this->pDisplay->getMaxCharHeight();
  uint8_t maxCharWidth = this->pDisplay->getMaxCharWidth();
  uint8_t messageBoxHeight =
      displayHeight - (maxCharHeight + 4) - this->mTopYPos;
  this->mTotalLineShown = messageBoxHeight / maxCharHeight;

  uint8_t line = 0;
  int lastSpacePos = 0;
  uint8_t bufferSize = displayWidth / maxCharWidth;
  char *buffer = new char[bufferSize];
  memset(buffer, 0, bufferSize);
  int charCopied = 0;
  uint8_t currentWidth = 0;

  for (int i = 0; i < messageLen; i++) {

    char currentChar = message[i];

    if (currentChar == '\n') {
      char *mline = new char[charCopied + 1];
      memset(mline, 0, charCopied + 1);
      memcpy(mline, buffer, charCopied);
      this->pMessageLine[line] = mline;
      memset(buffer, 0, bufferSize);
      charCopied = 0;
      line++;
      currentWidth = 0;
      continue;
    }

    if ((currentWidth + 6) > displayWidth) {
      charCopied -= i - lastSpacePos;
      char *mline = new char[charCopied + 1];
      memset(mline, 0, charCopied + 1);
      memcpy(mline, buffer, charCopied);
      this->pMessageLine[line] = mline;
      memset(buffer, 0, bufferSize);
      charCopied = 0;
      line++;
      i = lastSpacePos;
      currentWidth = 0;
      continue;
    }

    if (currentChar == ' ' && lastSpacePos != i)
      lastSpacePos = i;

    buffer[charCopied] = currentChar;
    charCopied++;
    char x[2] = {0, 0};
    x[0] = currentChar;
    currentWidth += this->pDisplay->getStrWidth(x);
  }

  if (charCopied > 0) {
    char *mline = new char[charCopied + 1];
    memset(mline, 0, charCopied + 1);
    memcpy(mline, buffer, charCopied);
    this->pMessageLine[line] = mline;
    memset(buffer, 0, bufferSize);
    line++;
  }

  delete[] buffer;

  this->mTotalLine = line;
  this->mStartLine = 0;
  this->mActiveOption = 0;
} // DialogUI::setMessage

void DialogUI::setOkLabel(const char *okLabel) {
  memset(this->mOkLabel, 0, DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH);
  strcpy(this->mOkLabel, okLabel);
} // DialogUI::setOkLabel

void DialogUI::setOkHandler(optionhandler_t handler) {
  this->mOkHandler = handler;
} // DialogUI::setOkHandler

void DialogUI::setCancelLabel(const char *cancelLabel) {
  memset(this->mCancelLabel, 0, DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH);
  strcpy(this->mCancelLabel, cancelLabel);
} // DialogUI::setCancelLabel

void DialogUI::setCancelHandler(optionhandler_t handler) {
  this->mCancelHandler = handler;
} // DialogUI::setCancelHandler
