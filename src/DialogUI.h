/**

   DialogUI.h

   Dialog-like UI to used with u8g2lib.h

   Copyright 2023 Ragil Rynaldo Meyer <meyer.ananda@gmail.com>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

*/

#ifndef DIALOG_UI_H
#define DIALOG_UI_H

#pragma once

#include <Arduino.h>
#include <U8g2lib.h>
#include <functional>
#include <string.h>

/**
   typedef optionhandler_t

   Function to call when execute option.

   @return void
*/
typedef std::function<void()> optionhandler_t;

#define DIALOG_UI_MAX_LINE_CHAR_LENGTH 20
#define DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH 9
#define DIALOG_UI_THICK_HEAD_ARROW_UP_ICON 112
#define DIALOG_UI_THICK_HEAD_ARROW_DOWN_ICON 109
#define DIALOG_UI_ALPHABET_FONT u8g2_font_profont12_mr
#define DIALOG_UI_ICON_FONT u8g2_font_open_iconic_all_1x_t

/**
   class DialogUI

   Rendering Dialog-like UI on display.
*/
class DialogUI {
public:
  /**
     DialogUI

     constructor for DialogUI

     @param [in] `*display` - pointer to display used.
     @param [in] `startYPos` - Y position to start render the UI.
  */
  DialogUI(U8G2 *display, uint8_t startYPos);
  // Use in loop to update the display.
  void loop();

  // Scroll back a long message or prev option.
  void prev();

  // Scroll next a long message or next option.
  void next();

  // Hide the UI.
  void hide();

  // Show the UI.
  void show();

  // Reset all values but show/hidden value.
  void reset();

  // Execute active selected option if available.
  void executeOption();

  /**
     setMessage

     Set message to show on dialog.

     @param [in] `message` - message to set on dialog.
  */
  void setMessage(const char *message);

  /**
     setOkLabel

     Set OK option label (bottom left side).

     @param [in] `okLabel` - label to set on OK option.
  */
  void setOkLabel(const char *okLabel);

  /**
     setOkHandler

     Set OK option handler to execute, NULL handler will be hidden.

     @param [in] `handler` - handler to execute on OK option.
  */
  void setOkHandler(optionhandler_t handler);

  /**
     setCancelLabel

     Set Cancel option label (bottom right side).

     @param [in] `cancelLabel` - label to set on Cancel option.
  */
  void setCancelLabel(const char *cancelLabel);

  /**
     setCancelHandler

     Set Cancel option handler to execute, NULL handler will be hidden.

     @param [in] `handler` - handler to execute on Cancel option.
  */
  void setCancelHandler(optionhandler_t handler);

private:
  // variable to hold char for OK option label.
  char mOkLabel[DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH] = "ok";

  // variable to hold char for Cancel option label.
  char mCancelLabel[DIALOG_UI_MAX_BUTTON_LABEL_CHAR_LENGTH] = "cancel";

  // variable to hold char for OK option handler.
  optionhandler_t mOkHandler = NULL;

  // variable to hold char for Cancel option handler.
  optionhandler_t mCancelHandler = NULL;

  // variable to determined if UI should be hidden.
  bool mHidden = false;

  // variable to hold active option (highlighted).
  int8_t mActiveOption = 0;

  // variabel to tell start of index line to show (if message is long).
  uint8_t mStartLine = 0;

  // variabel to tell how many line message will be fit on display.
  uint8_t mTotalLine = 0;

  // variabel to hold display object.
  U8G2 *pDisplay = NULL;

  // variabel to tell start of top display to render.
  uint8_t mTopYPos = 0;

  // variabel to tell how many line will be shown at once based on display and
  // font used.
  uint8_t mTotalLineShown = 0;

  // variabel to hodl char for message shown on dialog.
  char *pMessageLine[DIALOG_UI_MAX_LINE_CHAR_LENGTH] = {};
}; // class DialogUI

#endif

// Local Variables:
// mode: c++
// End:
